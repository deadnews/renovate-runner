module.exports = {
    autodiscover: true,
    autodiscoverTopics: ['renovate-managed'],
    persistRepoData: true,
    platform: 'gitlab',
};
